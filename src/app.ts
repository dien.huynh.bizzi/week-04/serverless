// import fastify, { FastifyServerOptions } from "fastify";
// import cors from "@fastify/cors";

// const App = (options: FastifyServerOptions): any => {
//   const app = fastify(options);
//   // (async () => {
//   //   console.log("---------cho fastify-------")
//   //   app.ready();
//   // })();

//   // app.register(cors, {
//   //   origin: ["http://localhost:3000", "https://week-03-fe.vercel.app"],
//   //   methods: ["GET", "PUT", "PATCH", "POST", "DELETE"],
//   //   credentials: true,
//   // });

//   // const apollo = new ApolloServer<BaseContext>({
//   //   typeDefs,
//   //   resolvers,
//   //   plugins: [fastifyApolloDrainPlugin(app)],
//   // });

//   (async () => {
//     // await createConnection();
//     // await apollo.start();
//   })();

//   // app.register(async (app: FastifyInstance) => {
//   //   console.log("------------------ vao day ----------------")
//   //   app.route({
//   //     url: "/graphql",
//   //     method: ["GET", "POST", "OPTIONS"],
//   //     handler: fastifyApolloHandler(apollo, {
//   //       context: authContextFunction,
//   //     }),
//   //   });
//   // });
//   app.post("/graphql", (request, reply) => {
//     // console.log("request", request)
//     return reply.send({ hello: "post world" });
//   });

//   return app;
// };

// export default App;

// import fastify, { FastifyReply, FastifyRequest } from "fastify";
// import cors from "@fastify/cors";

// const app = fastify({ logger: true });

// app.register(cors, {
//   origin: "http://localhost:3000",
//   methods: ["GET", "POST"],
//   credentials: true,
// });

// // app.post("/graphql", (request: FastifyRequest, reply: FastifyReply) => {
// //   reply.send({ hello: "world" });
// // });

// // app.get("/temp", (request: FastifyRequest, reply: FastifyReply) => {
// //   reply.send({ hello: "temp" });
// // });

// export default app;

// import { ApolloServer, BaseContext } from "@apollo/server";
// import fastify, { FastifyInstance } from "fastify";
// import { resolvers, typeDefs } from "./graphql";
// import { fastifyApolloDrainPlugin, fastifyApolloHandler } from "@as-integrations/fastify";
// import { AuthContext, authContextFunction } from "./graphql/contexts/auth-context";
// import { createConnection } from "./graphql/config/db";

// function init() {
//   const app = fastify();

//   const apollo = new ApolloServer<AuthContext>({
//     typeDefs,
//     resolvers,
//     plugins: [fastifyApolloDrainPlugin(app)],
//   });

//   // const handler = startServerAndCreateLambdaHandler(apollo, handlers.createAPIGatewayProxyEventV2RequestHandler());

//   // // apollo.start().then(() => {
//   (async () => {
//     // await app.ready();
//     await createConnection();
//     await apollo.start();
//   })();
//   // app.register(fastifyApollo(apollo), {
//   //   // context: async (request: any, reply: any) => ({
//   //   //   // isAuthenticated: authMiddleware(request),
//   //   //   // isAdmin: isAdmin(request),
//   //   //   reply,
//   //   //   request,
//   //   // }),
//   // });
//   app.register(async (app: FastifyInstance) => {
//     app.route({
//       url: "/graphql",
//       method: ["GET", "POST", "OPTIONS"],
//       handler: fastifyApolloHandler(apollo, {
//         context: authContextFunction,
//       }),
//     });
//   });
//   app.get("/", (request, reply) => reply.send({ hello: "world" }));
//   return app;
// }

// if (require.main === module) {
//   // called directly i.e. "node app"
//   init().listen({ port: 3000 }, (err) => {
//     if (err) console.error(err);
//     console.log("server listening on 3000");
//   });
// }
// // else {
// // required as a module => executed on aws lambda
// export default init;
// }

import fastify, { FastifyInstance, FastifyServerOptions } from "fastify";
import { ApolloServer } from "@apollo/server";
import { fastifyApolloHandler, fastifyApolloDrainPlugin } from "@as-integrations/fastify";
import { AuthContext, authContextFunction } from "./graphql/contexts/auth-context";
import { resolvers, typeDefs } from "./graphql";
import { createConnection } from "./graphql/config/db";

const App = (options: FastifyServerOptions) => {
  const app = fastify(options);

  const apollo = new ApolloServer<AuthContext>({
    typeDefs,
    resolvers,
    plugins: [fastifyApolloDrainPlugin(app)],
  });

  (async () => {
    await createConnection();
    await apollo.start();
  })();

  app.register(async (app: FastifyInstance) => {
    app.route({
      url: "/graphql",
      method: ["POST", "OPTIONS"],
      handler: fastifyApolloHandler(apollo, {
        context: authContextFunction,
      }),
    });
  });
  app.get("/", (request, reply) => reply.send({ hello: "world" }));
  
  return app;
};
export default App;
