// import { FastifyInstance, FastifyServerOptions } from "fastify";
// import App from "./app";
// import { createConnection } from "./graphql/config/db";

// import { ApolloServer, BaseContext } from "@apollo/server";
// import { resolvers, typeDefs } from "./graphql";
// import fastifyApollo, { fastifyApolloDrainPlugin, fastifyApolloHandler } from "@as-integrations/fastify";
// import app from "./app";
// import { handlers, startServerAndCreateLambdaHandler } from "@as-integrations/aws-lambda";
// import { FastifyInstance } from "fastify";
// import { AuthContext, authContextFunction } from "./graphql/contexts/auth-context";

// const init = () => {
//   const options: FastifyServerOptions = {
//     logger: true,
//   };

//   const app: FastifyInstance = App(options);

//   (async () => {
//     await app.ready();
//     // await createConnection();
//     // await apollo.start();
//   })();

//   return app;
// };

// if (require.main === module) {
//   init().listen({ port: 3000 }, (err) => {
//     if (err) console.error(err);
//     console.log("server listening on 3000");
//   });
// }

// export default init;

//----------------
// const apollo = new ApolloServer<BaseContext>({
//   typeDefs,
//   resolvers,
//   plugins: [fastifyApolloDrainPlugin(app)],
// });

// const handler = startServerAndCreateLambdaHandler(apollo, handlers.createAPIGatewayProxyEventV2RequestHandler());

// // apollo.start().then(() => {
//   // (async () => {
//   //   // await app.ready();
//   //   // await createConnection();
//   //   await apollo.start();
//   // })();
// app.register(fastifyApollo(apollo), {
//   // context: async (request: any, reply: any) => ({
//   //   // isAuthenticated: authMiddleware(request),
//   //   // isAdmin: isAdmin(request),
//   //   reply,
//   //   request,
//   // }),
// });
//   // app.register(async (app: FastifyInstance) => {
//   //   app.route({
//   //     url: "/graphql",
//   //     method: ["GET", "POST", "OPTIONS"],
//   //     handler: fastifyApolloHandler(apollo, {
//   //       context: authContextFunction,
//   //     }),
//   //   });
//   // });
// // app.listen({ port: myPort }, async (err: Error | null, address: string) => {
// //   if (err) {
// //     console.error(err);
// //     process.exit(1);
// //   }
// //   console.log(`Server is listening at ${address}`);
// // });
// // });

// export { handler };
// export default apollo;

import { ApolloServer, BaseContext } from "@apollo/server";
import fastify, { FastifyInstance, FastifyServerOptions } from "fastify";
import { resolvers, typeDefs } from "./graphql";
import { fastifyApolloDrainPlugin, fastifyApolloHandler } from "@as-integrations/fastify";
import { AuthContext, authContextFunction } from "./graphql/contexts/auth-context";
import { createConnection } from "./graphql/config/db";
import config from "./graphql/config/env";
import App from "./app";

function init() {
  // const app = fastify();
  const options: FastifyServerOptions = {
    logger: true,
  };

  const app: FastifyInstance = App(options);

  // const apollo = new ApolloServer<AuthContext>({
  //   typeDefs,
  //   resolvers,
  //   plugins: [fastifyApolloDrainPlugin(app)],
  // });

  // const handler = startServerAndCreateLambdaHandler(apollo, handlers.createAPIGatewayProxyEventV2RequestHandler());

  // // apollo.start().then(() => {
  // (async () => {
  //   // await app.ready();
  //   await createConnection();
  //   await apollo.start();
  // })();
  // app.register(fastifyApollo(apollo), {
  //   // context: async (request: any, reply: any) => ({
  //   //   // isAuthenticated: authMiddleware(request),
  //   //   // isAdmin: isAdmin(request),
  //   //   reply,
  //   //   request,
  //   // }),
  // });
  // app.register(async (app: FastifyInstance) => {
  //   app.route({
  //     url: "/graphql",
  //     method: ["GET", "POST", "OPTIONS"],
  //     handler: fastifyApolloHandler(apollo, {
  //       context: authContextFunction,
  //     }),
  //   });
  // });
  // app.get("/", (request, reply) => reply.send({ hello: "world" }));
  return app;
}

console.log("-------require.main", require.main);
if (require.main === module) {
  const PORT: string | number = +config.port;
  // called directly i.e. "node app"
  init().listen({ port: PORT }, (err) => {
    if (err) console.error(err);
    console.log("server listening on 3000");
  });
}
// else {
// required as a module => executed on aws lambda
export default init;
