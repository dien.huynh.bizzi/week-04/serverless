// export const hello = async (event, context) => {
//   return {
//     statusCode: 200,
//     body: JSON.stringify({
//       message: `Go Serverless v2.0! ${await message({ time: 1, copy: "Your function executed successfully!" })}`,
//     }),
//   };
// };

// const message = ({ time, ...rest }) =>
//   new Promise((resolve, reject) =>
//     setTimeout(() => {
//       resolve(`${rest.copy} (with a delay)`);
//     }, time * 1000)
//   );

// import awsLambdaFastify from "@fastify/aws-lambda";
// import init from "../index";
// import { ApolloServer, ApolloServerOptions } from "@apollo/server";
// import { resolvers, typeDefs } from "../graphql";

// const server = new ApolloServer({
//   typeDefs,
//   resolvers,
//   context: ({ event, context }) => ({
//     headers: event.headers,
//     functionName: context.functionName,
//     event,
//     context,
//   }),
//   // playground: {
//   //   endpoint: "/production/graphql",
//   // },
//   // tracing: true,
// });

// const proxy = awsLambdaFastify(init());

// export const handler = async (event, context) => {
//   context.callbackWaitsForEmptyEventLoop = false;
//   // console.log("event", event);
//   // console.log("context", context);
//   return await proxy(event, context);
// };

import { awsLambdaFastify } from "@fastify/aws-lambda";
import init from "..";
// import { handler as apolloHandler } from "..";

const proxy = awsLambdaFastify(init());

// export async function handler(event: any, context: any, callback: any) {
//   const res =  await proxy(event, context);
//   console.log("---res---", res)
//   // return res
//   // console.log("--------vao handler-------------")
//   switch (event.path) {
//     case "/":

//   //     console.log("--------vao case res-------------")
//       return res;
//     case "/graphql":
//       // return res
//   //     console.log("----------vao case graphql------------");
//   //     const res1 = await proxy(event, context);
//   //     // console.log("=-----res1", res1)
//   //     // console.log("event", event);
//   //     // console.log("context", context);
//       return await apolloHandler(event, context, callback(null, res));
//   //     // return await apolloHandler(event, context);
//   }
// }

export const handler = async (event: any, context: any) => {
  const res = await proxy(event, context);
  console.log("---res---", res);
  return res;
};
