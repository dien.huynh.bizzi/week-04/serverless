import { getConnection } from "../config/db";
import config from "../config/env";
import {
  MSG_ERR_CREDENTIAL_IS_INCORRECT,
  MSG_ERR_INVALID_TOKEN,
  MSG_ERR_USER_IS_NOT_EXIST,
  MSG_NEW_TOKEN,
} from "../../constants/message";
import jwt from "jsonwebtoken";
import { compareString, createToken, hashString } from "../helpers/encode";
import { tokenEnum } from "../../constants/enum";

export const refresh = async (parent: any, args: any, contextValue: any, info: any) => {
  const { jwtRefesh } = args.user;

  let verify: any;
  try {
    verify = jwt.verify(jwtRefesh, config.jwt.refreshToken as string);
  } catch (err) {
    throw new Error(MSG_ERR_INVALID_TOKEN);
  }

  getConnection().read();
  const foundUser = getConnection().get("users").find({ id: verify.aud }).value();

  if (!foundUser) {
    throw new Error(MSG_ERR_USER_IS_NOT_EXIST);
  }

  const jwtRefeshSlice = jwtRefesh.slice(jwtRefesh.lastIndexOf("."));

  const isMatch = await compareString(jwtRefeshSlice, foundUser.refreshToken || "");

  if (!isMatch) {
    throw new Error(MSG_ERR_CREDENTIAL_IS_INCORRECT);
  }

  const accessToken = createToken(foundUser.id);

  const refreshToken = createToken(foundUser.id, tokenEnum.RefreshToken);

  const newRefreshToken = refreshToken.slice(refreshToken.lastIndexOf("."));

  const refreshTokenHash = await hashString(newRefreshToken);

  getConnection().get("users").find({ id: foundUser.id }).assign({ refreshToken: refreshTokenHash }).write();

  return {
    msg: MSG_NEW_TOKEN,
    AccessToken: accessToken,
    RefreshToken: refreshToken,
  };
};
