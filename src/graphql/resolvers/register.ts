import { getConnection } from "../config/db";
import { MSG_ERR_USERNAME_WAS_EXIST, MSG_REGISTER_SUCCESSFULLY } from "../../constants/message";
import { hashString } from "../helpers/encode";
import { v4 as uuidv4 } from "uuid";
import { User } from "../types/models/user";

export const register = async (parent: any, args: any, contextValue: any, info: any) => {
  console.log("------------trong register------------");
  // console.log("parent", parent)
  // console.log("args", args)
  // console.log("contextValue", contextValue)
  // console.log("info", info)
  const { username, password, email } = args.user;

  console.log("---------truoc founfUser register------------");
  // try {
    const foundUser = getConnection().get("users").find({ username }).value();
    // console.log("---foundUser", foundUser);
    if (foundUser) {
      throw new Error(MSG_ERR_USERNAME_WAS_EXIST);
    }
  // } catch (err) {
  //   console.log("---read db: " + err);
  // }

  const hashPassword = await hashString(password);

  const user: User = {
    id: uuidv4(),
    username,
    password: hashPassword,
    email,
    refreshToken: null,
    resetPass: null,
  };
  console.log("---------truoc getconnection register------------");
  // try {
    getConnection().get("users").push(user).write();
  // } catch (err) {
  //   console.log("---write db: " + err);
  // }

  console.log("---------tra kq register------------");

  return { msg: MSG_REGISTER_SUCCESSFULLY, id: user.id, username: user.username };
};
