import { getConnection } from "../config/db";
import { tokenEnum } from "../../constants/enum";
import {
  MSG_ERR_CREDENTIAL_IS_INCORRECT,
  MSG_ERR_USER_IS_NOT_EXIST,
  MSG_LOGIN_SUCCESSFULLY,
} from "../../constants/message";
import { compareString, createToken, hashString } from "../helpers/encode";

export const login = async (parent: any, args: any, contextValue: any, info: any) => {
  const { username, password, isRemember } = args.user;

  const foundUser = getConnection().get("users").find({ username }).value();

  if (!foundUser) {
    throw new Error(MSG_ERR_USER_IS_NOT_EXIST);
  }

  const isMatch = await compareString(password, foundUser.password);

  if (!isMatch) {
    throw new Error(MSG_ERR_CREDENTIAL_IS_INCORRECT);
  }

  const accessToken = createToken(foundUser.id);

  let refreshToken = "";

  if (isRemember) {
    refreshToken = createToken(foundUser.id, tokenEnum.RefreshToken);

    const newRefreshToken = refreshToken.slice(refreshToken.lastIndexOf("."));
    const refreshTokenHash = await hashString(newRefreshToken);

    getConnection().get("users").find({ id: foundUser.id }).assign({ refreshToken: refreshTokenHash }).write();
  }

  return {
    msg: MSG_LOGIN_SUCCESSFULLY,
    id: foundUser.id,
    username: foundUser.username,
    accessToken,
    refreshToken,
  };
};
