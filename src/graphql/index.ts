import { Resolvers } from "./resolvers";
import { TypeDefs } from "./typeDefs";

export const resolvers = {
  Query: {
    ...Resolvers.Query,
  },
  Mutation: {
    ...Resolvers.Mutation,
  },
};

export const typeDefs = TypeDefs;
