import { User } from "./user";

export type Data = {
  users: User[];
};
