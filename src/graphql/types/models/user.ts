export type User = {
    id: string;
    username: string;
    password: string;
    email: string;
    refreshToken: string | null;
    resetPass: string | null;
  };
  