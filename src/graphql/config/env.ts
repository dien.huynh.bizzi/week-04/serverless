import dotenv from "dotenv";

dotenv.config();

const config = {
  port: process.env.PORT || 5000,
  db: {
    file: process.env.DB_FILE || "src/graphql/config/db.json",
  },
  client: {
    host: process.env.CLIENTHOST || "http://localhost:3000",
  },
  jwt: {
    accessToken: process.env.JWT_SECRET || "bizzi123",
    accessTokenExpiresin: process.env.JWT_EXPIRESIN_SECRET || 30,
    refreshToken: process.env.JWT_REFRESH || "refreshbizzi123",
    refreshTokenExpiresin: process.env.JWT_EXPIRESIN_REFRESH || 3000,
    resetPasswordToken: process.env.JWT_RESETPASSWORD || "resetpassbizzi123",
    resetPasswordTokenExpiresin: process.env.JWT_EXPIRESIN_RESETPASSWORD || 300,
  },
  mail: {
    host: process.env.MAIL_HOST || "sandbox.smtp.mailtrap.io",
    port: process.env.MAIL_PORT || 2525,
    user: process.env.MAIL_USER || "1fb5e96b82b86c",
    password: process.env.MAIL_PASSWORD || "5224c7dfa336e1",
    mailFrom: process.env.MAIL_FROM || "<noreply@example.com>",
  },
};

export default config;
